#!/usr/bin/env ruby

require 'faker'
require 'rubyXL'
require 'rubyXL/convenience_methods'
require 'inifile'
require 'pp'

config = IniFile.load 'config.ini'

filename = config['generator']['xlsfile']

createexcel = config['generator']['createexcel'] == 1 ? true : false
deletedirs = config['generator']['deletedirs'] == 1 ? true : false
createfiles = config['generator']['createfiles'] == 1 ? true : false

dirs = config['input'].keys

if deletedirs
  puts "Deleting directories if needed and recreate them"
  dirs.each do |item|
    if Dir.exist?(item)
      FileUtils.rm_rf item
    end
    Dir.mkdir item
  end
else
  puts "Creating source directories if they don't exist"
  dirs.each do |item|
    if !Dir.exist?(item)
      Dir.mkdir item
    end
  end
end

if createexcel
  puts "Remove Excel file if it exists"
  if File.exist?(filename)
    File.delete(filename)
  end

  Faker::Config.locale = config['generator']['locale']
  count_of_rows = config['generator']['count'].to_i

  puts "Generating #{count_of_rows} rows with EANs and Matchcodes and files in directories"
  workbook = RubyXL::Workbook.new
  worksheet = workbook.worksheets[0]
  worksheet.add_cell(0, config['xlscols']['ean'], 'ean')
  worksheet.add_cell(0, config['xlscols']['matchcode'], 'matchcode')

  count_of_rows.times do |i|
    ean = Faker::Code.ean
    img_file = "#{ean}.jpg"
    matchcode = 'LG' + Faker::Number.number(digits: 11).to_s
    worksheet.add_cell(i, config['xlscols']['ean'], ean)
    worksheet.add_cell(i, config['xlscols']['matchcode'], matchcode)
  end

  workbook.write(filename)
end

puts "Reading Excel file"
workbook = RubyXL::Parser.parse(filename)
worksheet = workbook.worksheets[0]

go_on = true
lookup = {}
idx = config['filemerger']['skipheaders']

while go_on do
  row = worksheet[idx]
  if row.nil?
    go_on = false
  else
    if row[config['xlscols']['ean']].nil?
      matchcode = row[config['xlscols']['matchcode']].value
      puts "EAN missing for matchcode #{matchcode}"
    else
      ean = row[config['xlscols']['ean']].value
      matchcode = row[config['xlscols']['matchcode']].value
      lookup[ean] = matchcode
    end
    idx = idx + 1
  end
end

#pp lookup

if createfiles
  puts "Creating fake image files"
  lookup.each do |ean, matchcode|
    img_file = "#{ean}.jpg"
    dirs.each do |item|
      FileUtils.touch "#{item}/#{img_file}"
    end
  end
end

puts "Done."