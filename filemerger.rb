#!/usr/bin/env ruby

require 'rubyXL'
require 'rubyXL/convenience_methods'
require 'csv'
require 'inifile'
require 'pp'
require 'RMagick'
include Magick

config = IniFile.load 'config.ini'

xfilename = config['filemerger']['xlsfile']
$suffix = config['input']
$filetype = config['filemerger']['filetype']
$destfiletype = config['filemerger']['destfiletype']

$showwarnings = config['filemerger']['showwarnings'] == 1 ? true : false
$copynotmove = config['filemerger']['copynotmove'] == 1 ? true : false
splitfilenames = config['filemerger']['splitfilenames'] == 1 ? true : false
$convertimages = false

if config['filemerger']['destfiletype'].size > 0
  if config['filemerger']['filetype'] != config['filemerger']['destfiletype']
    $convertimages = true
  end
end

$indir = config['filemerger']['indir']
dirs = config['input'].keys

puts "Creating new directory"

$outdir = config['filemerger']['outdir']
if Dir.exist?($outdir)
  FileUtils.rm_rf $outdir
end
Dir.mkdir $outdir

def move_files(dir, ean, matchcode)
  src_file = get_filename_for_ean(dir, ean)
  dst_file = get_filename_for_matchcode(dir, matchcode)
  if $copynotmove
    FileUtils.cp(src_file, dst_file)
  else
    FileUtils.move(src_file, dst_file)
  end
  if $convertimages
    cnv_file = get_conv_filename(dir, matchcode)
    image = Image.read(dst_file).first
    image.write(cnv_file)
    FileUtils.rm_f(dst_file)
  end
end

def get_filename_for_ean(dir, ean)
  tmp = Dir["#{$indir}/#{dir}/#{ean}*.#{$filetype}"]
  result = ''
  if tmp.size == 1
    result = tmp[0]
  else
    if $showwarnings
      puts "More than one file found for EAN #{ean} in dir #{dir}"
    end
  end
  return result
end

def get_filename_for_matchcode(src_dir, matchcode)
  sfx = $suffix[src_dir]
  return "#{$outdir}/#{matchcode}#{sfx}.#{$filetype}"
end

def get_conv_filename(src_dir, matchcode)
  sfx = $suffix[src_dir]
  return "#{$outdir}/#{matchcode}#{sfx}.#{$destfiletype}"
end

puts "Reading Excel file"
workbook = RubyXL::Parser.parse(xfilename)
worksheet = workbook.worksheets[0]

go_on = true
lookup = {}
idx = config['filemerger']['skipheaders']

while go_on do
  row = worksheet[idx]
  if row.nil?
    go_on = false
  else
    if row[config['xlscols']['ean']].nil?
      matchcode = row[config['xlscols']['matchcode']].value
      if $showwarnings
        puts "EAN missing for matchcode #{matchcode}"
      end
    else
      ean = row[config['xlscols']['ean']].value
      matchcode = row[config['xlscols']['matchcode']].value
      lookup[ean] = matchcode
    end

    idx = idx + 1
  end
end

puts "Going through all the ean/matchcode combinations from the excel file and see if we find files to move/rename"

if $copynotmove
  puts "Only copying files, not moving"
end

if $convertimages
  puts "Converting images from #{$filetype} to #{$destfiletype}"
end

not_found = []

lookup.each do |ean, matchcode|
  dirs.each do |item|
    img_file = get_filename_for_ean(item, ean)
    if img_file.size > 0
      if File.exist?(img_file)
        move_files(item, ean, matchcode)
      else
        not_found << "#{item}: #{ean}"
      end
    end
  end
end

if not_found.size > 0
  puts "Files not found:"
  pp not_found
else
  puts "All files found"
end

puts "Done."