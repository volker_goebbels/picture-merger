# Image file merger

## Prerequisites

Needs a decent ruby installation. Installing rmagick on a Mac only works like this:
```bash
brew uninstall imagemagick
brew install imagemagick@6
export PATH="/usr/local/opt/imagemagick@6/bin:$PATH"
brew link --force imagemagick@6
gem install rmagick
```
Source: https://blog.francium.tech/installing-rmagick-on-osx-high-sierra-7ea71f57390d

## Installation

Invoke
```bash
bundle install
```
to install the dependencies in ruby gems.

Have a look at the `config.ini` file for some configuration options.

## Usage

### Fake data generator

Just call `./generator.rb` and it will generate an excel file with a
lookup table from EAN to matchcode and empty image files in all the
specified source directories.
It also creates the source directories after trying to delete them. 

### File mover

Invoke `./filemerger.rb` and it will move the files from the source
directories to the destination specified in the `config.ini` and rename them
from `<EAN>.jpg` to `<MATCHCODE>_<SUFFIX>.jpg`. You also can specify another
file extension (but files are not converted!).

## config.ini options

### Section xlscols

Define in which colmun (number) to find ean and matchcode.
```
ean=2
matchcode=0
```

### Section  input

Files from which subdirectory will get which suffix.
```
Front=_F_a
Top-W=_TS_a
Top-S=_TS_c
```

### Section generator

Options for the dummy data generator.
* `xlsfile` = name of the excel file to be generated
* `createexcel` = should I create an excel file or use the one that's already there?
* `deletedirs` = delete the directories before regenerating them?
* `createfiles` = should I create empty dummy files?
* `locale` = when generating data with Faker lib, which localization should I use?
* `count` = how many dummy files/excel rows should I create?
```
xlsfile=LanaGrossa_Sortiment_Prym_2020-02-11.xlsx
createexcel=0
deletedirs=0
createfiles=1
locale=de
count=5
```

### Section filemerger

Options for the file mover / renamer.
* `splitfilenames` = should I split the filenames (have the filenames already suffixes)?
* `xlsfile` = name of the excel file to be read for translation from ean to matchcode 
* `skipheaders` = How many lines from the top of the excel file should I skip?
* `outdir` = name of the directory where the output files should go
* `indir` = ame of the directory to read the files from
* `filetype` = file extension of the source files
* `destfiletype` = file extension of the destination files. If set and different from `filetype`, files will be converted
* `showwarnings` = should I give warning messages for files not found of matchcodes without EAN code? 
* `copynotmove` = should I just copy files and not move them? (useful for debugging because the sources files are not destroyed) 
```
splitfilenames=1
xlsfile=LanaGrossa_Sortiment_Prym_2020-02-11.xlsx
skipheaders=1
outdir=new
indir=Prym_2020_02_11_exp2
filetype=tif
destfiletype=png
showwarnings=0
copynotmove=1
```